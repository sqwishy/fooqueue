Usage
-----

Don't use this; it's broken.

How
---

A requirement was that, after each insert we would know how much of the queue
had become readable; or what was writable after a read. This isn't trivial
because operations can finish in not the order they started in. For example:

#. A begins a write, reserving some memory to use at X.
#. B begins a write, reserving some memory to use at Y.
#. B finishes writing, now Y has data. But Y must be read after X.
   Since X has not been written yet, Y isn't readable. So the number of
   readable elements cannot be incremented.
#. A finishes writing, now X and Y have data and can be read. Update the
   semaphore for the number of readable elements by two.

Solving this is a pain and the performance, even without this complexity, isn't
good enough. Atomic instructions are expensive and everyone ends up busting
everyone else's cache all the time. I think It's more lucrative to change your
program to reduce contention than come up with clever ways of managing it.
