CC = clang
C_FLAGS = -Wall
C_FLAGS_RELEASE = $(C_FLAGS) -DNDEBUG -O3
C_FLAGS_DEBUG = $(C_FLAGS) -g -O3

SRC = src
OUT = build
RELEASE = $(OUT)/release
DEBUG = $(OUT)/debug

.PHONY: default clean test

default: build/release/fooqueue.so

clean:
	rm -rf $(OUT)

munit:
	git clone https://github.com/nemequ/munit/

munit/munit.c: munit

%.so.asm: %.so
	objdump -d $< > $@

%/.d:
	mkdir -p $@

clean_release:
	rm -rf $(RELEASE)

test_release: $(RELEASE)/test_fooqueue
	$(RELEASE)/test_fooqueue

$(RELEASE)/test_fooqueue: $(SRC)/test_fooqueue.c $(RELEASE)/fooqueue.so munit/munit.c | $(RELEASE)/.d
	$(CC) $(C_FLAGS_RELEASE) -pthread -I. -o $@ $^

$(RELEASE)/fooqueue.a: $(SRC)/fooqueue.c | $(RELEASE)/.d
	$(CC) $(C_FLAGS_RELEASE) -Os -o $@ $^

$(RELEASE)/fooqueue.so: $(SRC)/fooqueue.c | $(RELEASE)/.d
	$(CC) $(C_FLAGS_RELEASE) -shared -fPIC -o $@ $^

$(RELEASE)/example: $(SRC)/example.c $(RELEASE)/fooqueue.so | $(RELEASE)/.d
	$(CC) $(C_FLAGS_RELEASE) -Wall -pthread -o $@ $^

clean_debug:
	rm -rf $(DEBUG)

test_debug: $(DEBUG)/test_fooqueue
	$(DEBUG)/test_fooqueue

$(DEBUG)/test_fooqueue: $(SRC)/test_fooqueue.c $(DEBUG)/fooqueue.so munit/munit.c | $(DEBUG)/.d
	$(CC) $(C_FLAGS_DEBUG) -pthread -I. -o $@ $^

$(DEBUG)/fooqueue.a: $(SRC)/fooqueue.c | $(DEBUG)/.d
	$(CC) $(C_FLAGS_DEBUG) -Os -o $@ $^

$(DEBUG)/fooqueue.so: $(SRC)/fooqueue.c | $(DEBUG)/.d
	$(CC) $(C_FLAGS_DEBUG) -shared -fPIC -o $@ $^

$(DEBUG)/example: $(SRC)/example.c $(DEBUG)/fooqueue.so | $(DEBUG)/.d
	$(CC) $(C_FLAGS_DEBUG) -Wall -pthread -o $@ $^
