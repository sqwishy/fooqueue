#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <sys/eventfd.h>

#include "fooqueue.h"

// I can't get this stupid thing to inline

#define INC_EVENTFD_SEM(fd) \
        write(fd, &(uint64_t){1}, sizeof(uint64_t))

#define DEC_EVENTFD_SEM(fd) \
        read(fd, &(uint64_t){1}, sizeof(uint64_t))


struct fooqueue* allocate_fooqueue(unsigned int capacity, int flags) {
    // The actual size of items[] is one more than the capacity that it can
    // actually hold. So if items[] is 5 elements large, we only ever fill up 4
    // of them at a time. So internal_capacity is one + the usable capacity.
    unsigned int internal_capacity = 1 + capacity;
    struct fooqueue* queue = malloc(sizeof *queue + internal_capacity * sizeof queue->items[0]);
    if (NULL == queue) {
        return NULL;
    } else {
        queue->flags = flags;
        queue->capacity = capacity;
        queue->get_cursor_internal = \
        queue->get_cursor = \
        queue->put_cursor_internal = \
        queue->put_cursor = &queue->items[0];
        queue->tail = &queue->items[internal_capacity - 1];

        int get_flags = flags & FOOQ_GET;
        if (get_flags & FOOQ_SEM_GET) {
            sem_init(&queue->get_sem, 0, 0);
        } else if (get_flags & FOOQ_FD_GET) {
            int efd_flags = (get_flags & FOOQ_FD_NONBLOCK) ? EFD_NONBLOCK : 0;
            queue->get_fd = eventfd(0, EFD_SEMAPHORE | efd_flags);
        } else {
            assert(get_flags == FOOQ_SPIN_GET);
        }

        int put_flags = flags & FOOQ_PUT;
        if (put_flags & FOOQ_SEM_PUT) {
            sem_init(&queue->put_sem, 0, queue->capacity);
        } else if (put_flags & FOOQ_FD_PUT) {
            int efd_flags = (put_flags & FOOQ_FD_NONBLOCK) ? EFD_NONBLOCK : 0;
            queue->put_fd = eventfd(0, EFD_SEMAPHORE | efd_flags);
            write(queue->put_fd, &queue->capacity, sizeof(uint64_t));
        } else {
            assert(put_flags == FOOQ_SPIN_PUT);
        }

        return queue;
    }
}

void deallocate_fooqueue(struct fooqueue* queue) {
    if (queue->flags & FOOQ_FD_GET) {
        close(queue->get_fd);
    } else if (queue->flags & FOOQ_SEM_GET) {
        sem_close(&queue->get_sem);
    }
    if (queue->flags & FOOQ_FD_PUT) {
        close(queue->put_fd);
    } else if (queue->flags & FOOQ_SEM_PUT) {
        sem_close(&queue->put_sem);
    }
    free(queue);
}

unsigned int fooqueue_level(struct fooqueue* queue) {
    void** get_cursor = queue->get_cursor;
    void** put_cursor = queue->put_cursor;
    if (get_cursor > put_cursor) {
        return put_cursor + queue->capacity + 1 - get_cursor;
    } else {
        return put_cursor - get_cursor;
    }
}

/* I would suggest you not call this if you feel that these conditionals impact
 * your application performance in the same way that not having a spoiler
 * affects the speed of your honda civic.
 * If you like -funroll-loops, make a version of this function that doesn't
 * check the flags and does exactly what it needs to do since you know what
 * flags you're using ahead of time. */
bool fooqueue_get(struct fooqueue* queue, void** thing) {
    if (queue->flags & FOOQ_SEM_GET) {
        sem_wait(&queue->get_sem);
    } else if (queue->flags & FOOQ_FD_GET) {
        if (DEC_EVENTFD_SEM(queue->get_fd) < 0) {
            return false;
        }
    }
    bool success = fooqueue_get_internal(queue, thing);
    if (queue->flags & FOOQ_SEM_PUT) {
        //assert(success);
        sem_post(&queue->put_sem);
    } else if (queue->flags & FOOQ_FD_PUT) {
        INC_EVENTFD_SEM(queue->put_fd);
    }
    return success;
}

bool fooqueue_get_internal(struct fooqueue* queue, void** thing) {
    void** get_current;
    void** get_next;
    while (1) {
        get_current = queue->get_cursor_internal;
        if (get_current == queue->put_cursor) {
            // The queue is empty.
            return false;
        }
        get_next = get_current == queue->tail ? &queue->items[0] : (get_current + 1);
        if (__sync_bool_compare_and_swap(&queue->get_cursor_internal, get_current, get_next)) {
            break;
        }
    };
    *thing = *get_current;
    while(!__sync_bool_compare_and_swap(&queue->get_cursor, get_current, get_next));
    return true;
}

bool fooqueue_put(struct fooqueue* queue, void* thing) {
    if (queue->flags & FOOQ_SEM_PUT) {
        sem_wait(&queue->put_sem);
    } else if (queue->flags & FOOQ_FD_PUT) {
        if (DEC_EVENTFD_SEM(queue->put_fd) < 0) {
            return false;
        }
    }
    bool success = fooqueue_put_internal(queue, thing);
    if (queue->flags & FOOQ_SEM_GET) {
        //assert(success);
        sem_post(&queue->get_sem);
    } else if (queue->flags & FOOQ_FD_GET) {
        INC_EVENTFD_SEM(queue->get_fd);
    }
    return success;
}

bool fooqueue_put_internal(struct fooqueue* queue, void* thing) {
    void** put_current;
    void** put_next;
    while (1) {
        put_current = queue->put_cursor_internal;
        put_next = put_current == queue->tail ? &queue->items[0] : (put_current + 1);
        if (put_next == queue->get_cursor) {
            // The queue is full.
            return false;
        }
        if (__sync_bool_compare_and_swap(&queue->put_cursor_internal, put_current, put_next)) {
            break;
        }
    };
    *put_current = thing;
    while(!__sync_bool_compare_and_swap(&queue->put_cursor, put_current, put_next));
    return true;
}
