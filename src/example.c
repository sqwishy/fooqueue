#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/select.h>
#include <pthread.h>

#include "fooqueue.h"

#define NUM_WORKER_PAIRS 3

struct workerargs {
    struct fooqueue* queue;
    int ops;
};

void *write_to_queue(void* args) {
    struct workerargs* context = (struct workerargs*)args;
    struct fooqueue* queue = context->queue;
    uint64_t ops = context->ops;
    while (--ops) {
        while(!fooqueue_put(queue, (void*)ops));
        //fprintf(stderr, "level=%u\n", (unsigned int)fooqueue_level(queue));
    };
    pthread_exit(NULL);
}

void *read_from_queue(void* args) {
    struct workerargs* context = (struct workerargs*)args;
    struct fooqueue* queue = context->queue;
    uint64_t ops = context->ops;
    void* value;
    while (--ops) {
        while(!fooqueue_get(queue, &value));
#if NUM_WORKER_PAIRS < 1
        fprintf(stderr, "got: %i, expected: %i\n", value, ops);
        assert(value == ops);
#endif
    };
    pthread_exit(NULL);
}

// Having a different number of senders and receivers might not work at this
// point because of the example is designed to run ...
#define NUM_SENDERS NUM_WORKER_PAIRS
#define NUM_RECEIVERS NUM_WORKER_PAIRS

int main(int argc, char* argv[]) {
    struct fooqueue* queue = allocate_fooqueue(NUM_WORKER_PAIRS * 16, FOOQ_SEM);
    pthread_t senders[NUM_SENDERS];
    pthread_t receivers[NUM_RECEIVERS];

    struct workerargs args;
    args.queue = queue;
    args.ops = 10 * 1000;

    fprintf(stderr, "Putting and getting %i times per %i worker pair!\n", args.ops, NUM_SENDERS);

    clock_t start = clock();

    int i;
    for (i = 0; i < NUM_RECEIVERS; ++i) {
        pthread_create(&receivers[i], NULL, write_to_queue, &args);
    };
    for (i = 0; i < NUM_SENDERS; ++i) {
        pthread_create(&senders[i], NULL, read_from_queue, &args);
    };
    for (i = 0; i < NUM_RECEIVERS; ++i) {
        pthread_join(receivers[i], NULL);
    };
    for (i = 0; i < NUM_SENDERS; ++i) {
        pthread_join(senders[i], NULL);
    };

    clock_t end = clock();
    float seconds = (float)(end - start) / CLOCKS_PER_SEC / NUM_WORKER_PAIRS / 2;
    fprintf(stderr, "Completed in %f seconds.\n", seconds);

    return EXIT_SUCCESS;
}
