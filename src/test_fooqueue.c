#include <pthread.h>

#include <munit/munit.h>

#include "fooqueue.h"

static void*
test_fooqueue_setup_spin(const MunitParameter params[], void* user_data) {
  return (void*) allocate_fooqueue(4, FOOQ_SPIN);
}

static void*
test_fooqueue_setup_semaphore(const MunitParameter params[], void* user_data) {
  return (void*) allocate_fooqueue(4, FOOQ_SEM);
}

static void*
test_fooqueue_setup_blocking_fd(const MunitParameter params[], void* user_data) {
  return (void*) allocate_fooqueue(4, FOOQ_FD_BLOCK);
}

static void*
test_fooqueue_setup_nonblocking_fd(const MunitParameter params[], void* user_data) {
  return (void*) allocate_fooqueue(4, FOOQ_FD_NONBLOCK);
}

static void
test_fooqueue_teardown(void* fixture) {
  deallocate_fooqueue((struct fooqueue*)fixture);
}

static MunitResult
test_in_order(const MunitParameter params[], void* fixture) {
  struct fooqueue* queue = (struct fooqueue*) fixture;

  int puts[2] = {123, 456};
  void* gets[2] = {};
  fooqueue_put(queue, &puts[0]);
  fooqueue_put(queue, &puts[1]);
  fooqueue_get(queue, &gets[0]);
  munit_assert_ptr_equal(&puts[0], gets[0]);
  fooqueue_get(queue, &gets[1]);
  munit_assert_ptr_equal(&puts[1], gets[1]);

  return MUNIT_OK;
}

static MunitResult
test_level(const MunitParameter params[], void* fixture) {
  struct fooqueue* queue = (struct fooqueue*) fixture;

  void* thing;
  munit_assert_uint64(fooqueue_level(queue), ==, 0);
  fooqueue_put(queue, &thing);
  munit_assert_uint64(fooqueue_level(queue), ==, 1);
  fooqueue_get(queue, &thing);
  munit_assert_uint64(fooqueue_level(queue), ==, 0);
  fooqueue_put(queue, &thing);
  munit_assert_uint64(fooqueue_level(queue), ==, 1);
  fooqueue_put(queue, &thing);
  munit_assert_uint64(fooqueue_level(queue), ==, 2);
  fooqueue_put(queue, &thing);
  munit_assert_uint64(fooqueue_level(queue), ==, 3);
  fooqueue_put(queue, &thing);
  munit_assert_uint64(fooqueue_level(queue), ==, 4);

  return MUNIT_OK;
}

static MunitResult
test_capacity_nonblocking(const MunitParameter params[], void* fixture) {
  struct fooqueue* queue = (struct fooqueue*) fixture;

  char* puts[5] = {"spam", "eggs", "foo", "bar", "baz"};

  munit_assert_true(fooqueue_put(queue, &puts[0]));
  munit_assert_true(fooqueue_put(queue, &puts[1]));
  munit_assert_true(fooqueue_put(queue, &puts[2]));
  munit_assert_true(fooqueue_put(queue, &puts[3]));
  // Can't put anything in
  munit_assert_false(fooqueue_put(queue, &puts[4]));

  void* get;
  munit_assert_true(fooqueue_get(queue, &get));
  munit_assert_ptr_equal(get, &puts[0]);
  munit_assert_true(fooqueue_get(queue, &get));
  munit_assert_ptr_equal(get, &puts[1]);
  munit_assert_true(fooqueue_get(queue, &get));
  munit_assert_ptr_equal(get, &puts[2]);
  munit_assert_true(fooqueue_get(queue, &get));
  munit_assert_ptr_equal(get, &puts[3]);
  munit_assert_false(fooqueue_get(queue, &get));

  return MUNIT_OK;
}

static MunitResult
test_repeated(const MunitParameter params[], void* fixture) {
  struct fooqueue* queue = (struct fooqueue*) fixture;
  void* thing;
  int i;
  for (i = 0; i < 100 * 1000; i++) {
      munit_assert_true(fooqueue_put(queue, &thing));
      munit_assert_true(fooqueue_get(queue, &thing));
  };
  return MUNIT_OK;
}

/* Creating a test suite is pretty simple.  First, you'll need an
 * array of tests: */
static MunitTest test_suite_tests[] = {
  {
    (char*) "/fooqueue/nonblockfd/in_order",
    test_in_order,
    test_fooqueue_setup_blocking_fd,
    test_fooqueue_teardown,
    MUNIT_TEST_OPTION_NONE,
    NULL
  },
  {
    (char*) "/fooqueue/spin/level",
    test_level,
    test_fooqueue_setup_spin,
    test_fooqueue_teardown,
    MUNIT_TEST_OPTION_NONE,
    NULL
  },
  {
    (char*) "/fooqueue/spin/in_order",
    test_in_order,
    test_fooqueue_setup_spin,
    test_fooqueue_teardown,
    MUNIT_TEST_OPTION_NONE,
    NULL
  },
  {
    (char*) "/fooqueue/nonblockfd/capacity",
    test_capacity_nonblocking,
    test_fooqueue_setup_nonblocking_fd,
    test_fooqueue_teardown,
    MUNIT_TEST_OPTION_NONE,
    NULL
  },
  {
    (char*) "/fooqueue/spin/repeated",
    test_repeated,
    test_fooqueue_setup_spin,
    test_fooqueue_teardown,
    MUNIT_TEST_OPTION_NONE,
    NULL
  },
  {
    (char*) "/fooqueue/semaphore/repeated",
    test_repeated,
    test_fooqueue_setup_semaphore,
    test_fooqueue_teardown,
    MUNIT_TEST_OPTION_NONE,
    NULL
  },
  {
    (char*) "/fooqueue/nonblockfd/repeated",
    test_repeated,
    test_fooqueue_setup_nonblocking_fd,
    test_fooqueue_teardown,
    MUNIT_TEST_OPTION_NONE,
    NULL
  },
  {
    (char*) "/fooqueue/blockfd/repeated",
    test_repeated,
    test_fooqueue_setup_blocking_fd,
    test_fooqueue_teardown,
    MUNIT_TEST_OPTION_NONE,
    NULL
  },
  /* To tell the test runner when the array is over, just add a NULL
   * entry at the end. */
  { NULL, NULL, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL }
};

/* Now we'll actually declare the test suite.  You could do this in
 * the main function, or on the heap, or whatever you want. */
static const MunitSuite test_suite = {
  /* This string will be prepended to all test names in this suite;
   * for example, "/example/rand" will become "/�nit/example/rand".
   * Note that, while it doesn't really matter for the top-level
   * suite, NULL signal the end of an array of tests; you should use
   * an empty string ("") instead. */
  (char*) "",
  /* The first parameter is the array of test suites. */
  test_suite_tests,
  /* In addition to containing test cases, suites can contain other
   * test suites.  This isn't necessary in this example, but it can be
   * a great help to projects with lots of tests by making it easier
   * to spread the tests across many files.  This is where you would
   * put "other_suites" (which is commented out above). */
  NULL,
  /* An interesting feature of �nit is that it supports automatically
   * running multiple iterations of the tests.  This is usually only
   * interesting if you make use of the PRNG to randomize your tests
   * cases a bit, or if you are doing performance testing and want to
   * average multiple runs.  0 is an alias for 1. */
  1,
  /* Just like MUNIT_TEST_OPTION_NONE, you can provide
   * MUNIT_SUITE_OPTION_NONE or 0 to use the default settings. */
  MUNIT_SUITE_OPTION_NONE
};

/* This is only necessary for EXIT_SUCCESS and EXIT_FAILURE, which you
 * *should* be using but probably aren't (no, zero and non-zero don't
 * always mean success and failure).  I guess my point is that nothing
 * about �nit requires it. */
#include <stdlib.h>

int main(int argc, char* argv[MUNIT_ARRAY_PARAM(argc + 1)]) {
  /* Finally, we'll actually run our test suite!  That second argument
   * is the user_data parameter which will be passed either to the
   * test or (if provided) the fixture setup function. */
  return munit_suite_main(&test_suite, (void*) "�nit", argc, argv);
}
