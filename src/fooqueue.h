#pragma once

#include <stdint.h>
#include <stdbool.h>
#include <semaphore.h>

enum fooqueueflags {
    // Specify which built-in synchronization objects to use. You don't have to
    // use the same for both put() and get() operations.
    // You can probably also just use your own; initialize this with FOOQ_SPIN
    // and use the _internal functions.
    FOOQ_GET             = 0x00ff,
    FOOQ_PUT             = 0xff00,

    // Use no synchronization object. Let your yes be yes and your no be no.
    // get() and put() will return false iff nothing is available to take out
    // or there is no room to put something in; you'll just have to try again.
    // TODO this might be misleading because fooqueue does not do a spin lock
    // itself ... it's designed so you can I guess.
    FOOQ_SPIN            = 0x0000,
    FOOQ_SPIN_GET        = FOOQ_SPIN & FOOQ_GET,
    FOOQ_SPIN_PUT        = FOOQ_SPIN & FOOQ_PUT,
    // Use a posix semaphore. get() and put() will block until it can succeed.
    FOOQ_SEM             = 0x0101,
    FOOQ_SEM_GET         = FOOQ_SEM & FOOQ_GET,
    FOOQ_SEM_PUT         = FOOQ_SEM & FOOQ_PUT,
    // Use a blocking eventfd object. The file descriptor to select on lives at
    // put_fd and get_fd for FOOQ_FD_*_PUT and FOOQ_FD_*_GET respectively.
    // That is:
    // when put_fd is writable, you can put onto the queue with fooqueue_put();
    // when get_fd is readable, you can take off the queue with fooqueue_get().
    FOOQ_FD_BLOCK        = 0x0202,
    FOOQ_FD_BLOCK_GET    = FOOQ_FD_BLOCK & FOOQ_GET,
    FOOQ_FD_BLOCK_PUT    = FOOQ_FD_BLOCK & FOOQ_PUT,
    // Use a non-blocking eventfd object.
    FOOQ_FD_NONBLOCK     = 0x0404,
    FOOQ_FD_NONBLOCK_GET = FOOQ_FD_NONBLOCK & FOOQ_GET,
    FOOQ_FD_NONBLOCK_PUT = FOOQ_FD_NONBLOCK & FOOQ_PUT,

    FOOQ_FD_GET          = FOOQ_FD_BLOCK_GET | FOOQ_FD_NONBLOCK_GET,
    FOOQ_FD_PUT          = FOOQ_FD_BLOCK_PUT | FOOQ_FD_NONBLOCK_PUT,
    FOOQ_FD              = FOOQ_FD_PUT | FOOQ_FD_GET,
};

/* This is almost all internal; except the synchronization objects. */
struct fooqueue {
    int flags;
    int get_fd;
    int put_fd;
    sem_t get_sem;
    sem_t put_sem;
    unsigned int capacity;
    void* tail; // Tail end of items[]; to know when to wrap the cursors.
    void** get_cursor_internal;
    void** get_cursor;
    void** put_cursor_internal;
    void** put_cursor;
    void* items[];
};

/* The fooqueue is a FIFO queue. It's fixed-size multi-producer multi-consumer.
 * And it's thread-safe probably I hope.
 *
 * All the elements in the queue are just void pointers. The idea is that you
 * have some place in memory where you have a message and you want to send it
 * to someone else, so you put a pointer to that message in this queue. Then
 * the guy who reads off the queue knows where to look for the message.
 *
 * That means you don't want to free the message after putting it the pointer
 * to it onto the queue. This is just using the queue to pass messages or data
 * around by copying pointers. It's cheap, but your application is responsible
 * for managing the memory pointed to by queue elements. */

/* Allocate a fooqueue that can hold `capacity` items. */
struct fooqueue* allocate_fooqueue(unsigned int capacity, int flags);
/* Free everything allocated by allocate_fooqueue(). */
void deallocate_fooqueue(struct fooqueue* queue);

/* Oh man, what even is this? "level"? Who named this thing ... Is this the
 * number of items in the queue? That sounds like it might be right. It's
 * either that or how much you can still put into the queue. Who knows? */
unsigned int fooqueue_level(struct fooqueue* queue);

/* Take something off the queue. That is, set `thing` to point to the memory
 * pointed to by an element. Without a synchronization object, this will return
 * false if the queue is empty. */
bool fooqueue_get(struct fooqueue* queue, void** thing);
/* Take something off the queue without modifying any put or get
 * synchronization objects. This is equivalent (slightly cheaper) to
 * fooqueue_get() for a fooqueue initialized with FOOQ_SPIN.
 * Use this if you are doing your own synchronization or just want to watch the
 * world burn. */
bool fooqueue_get_internal(struct fooqueue* queue, void** thing);

/* Put something onto the queue. That is, make an element in the queue point to
 * memory `thing`. Without a synchronization object, this will return false if
 * the queue is full. */
bool fooqueue_put(struct fooqueue* queue, void* thing);
/* Put something onto the queue without modifying any put or get
 * synchronization objects. This is equivalent (slightly cheaper) to
 * fooqueue_put() for a fooqueue initialized with FOOQ_SPIN.
 * Use this if you are doing your own synchronization or just want to watch the
 * world burn. */
bool fooqueue_put_internal(struct fooqueue* queue, void* thing);
